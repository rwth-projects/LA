package la.uebung_3;

/**
 * Created by Marc Schmidt on 21.05.2017.
 */
public class BruteForce {

	public static void main(String[] args) {
		int corpse = 7;
		for (int a = 0; a < corpse; a++) {
			for (int v1 = 0; v1 < corpse; v1++) {
				for (int v2 = 0; v2 < corpse; v2++) {
					if (calc1(a, v1, v2) != calc2(a, v1, v2)) {
						System.out.println(a + ", " + v1 + ", " + v2 + ", " + calc1(a, v1, v2) + "\t" + calc2(a, v1, v2));
					}
				}
			}
		}
	}

	private static int calc1(int pA, int pV1, int pV2) {
		return 3 * (pA * pV1 + pV2) + 2;
	}

	private static int calc2(int pA, int pV1, int pV2) {
		return 3 * pA * pV1 + 2 + 3 * pV2 + 2;
	}

}
