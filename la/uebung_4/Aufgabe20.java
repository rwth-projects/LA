package la.uebung_4;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 03.06.2017.
 */
public class Aufgabe20 {

	public static void main(String[] args) {
		psi(3, 0, 0, -2);

		int[][] v = new int[][] {{4, -5, 3}, {-5, 2, -2}, {1, -2, 1}};
		for (int i = 0; i < v.length; i++) {
			int[] ints = v[i];
			int[] alpha = compPeta(compTheta(compPsi(ints)));
			System.out.println("v_" + i + ", a(" + Arrays.toString(ints) + ")=" + Arrays.toString(alpha) + ", modulo: " + Arrays.toString(mod(alpha,
					11)));
		}
	}

	private static void psi(int a, int b, int c, int d) {
		System.out.println((a + 3 * b + 2 * c + d) + "\t" + (-2 * a + 2 * b + 3 * c - 3 * d));
		System.out.println((-2 * b + c) + "\t" + (-2 * b - c - 2 * d));
	}

	private static int[] compPsi(int[] pArr) {
		return new int[] {-2 * pArr[0] + 1 * pArr[1] - 1 * pArr[2], -3 * pArr[0] + 1 * pArr[1] + 3 * pArr[2], 3 * pArr[0] + 4 * pArr[1]};
	}

	private static int[] compTheta(int[] pArr) {
		return new int[] {3 * pArr[0] - 3 * pArr[1], 1 * pArr[0] - 5 * pArr[1] + 4 * pArr[2], 5 * pArr[0] + 2 * pArr[1] + 4 * pArr[2]};
	}

	private static int[] compPeta(int[] pArr) {
		return new int[] {5 * pArr[0] - 5 * pArr[1] - 5 * pArr[2], -2 * pArr[0] + 1 * pArr[1], -1 * pArr[0] + 3 * pArr[1] + 2 * pArr[2]};
	}

	private static int[] mod(int[] pArr, int pModulo) {
		int[] arr = new int[pArr.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = pArr[i] % pModulo;
		}
		return arr;
	}
}
