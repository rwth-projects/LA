package la.uebung_7;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 03.07.2017.
 */
public class Add {

	private static int[][] sMatA = new int[][] {{5, 11, 4, 9}, {12, 1, 6, 14}, {-1, -20, -4, -6}, {3, -11, 0, 0}};
	private static int[][] sMatB = new int[][] {{-1, 0, 3, -7}, {10, -1, 6, 9}, {7, -2, -2, 16}, {7, 0, 1, 12}};
	private static int[][] sMatC = new int[][] {{6, -8, 11, -11}, {12, -18, 12, -6}, {-3, 8, -14, 22}, {1, -6, -3, 4}};
	private static int[][] sMatC_ = new int[][] {{-2, 1, 0, 0}, {0, -3, 0, 0}, {0, 0, -1, 0}, {0, 0, 0, 1}};
	private static int[][] sMatD = new int[][] {{5, 15, 4, 11}, {8, 5, 6, 8}, {-2, -28, -4, -12}, {0, -11, 0, -6}};
	private static int[][] sMatD_ = new int[][] {{1, 0, -1, 0}, {0, 2, 0, 0}, {0, 0, -2, 0}, {0, 0, 0, 1}};

	public static void main(String[] args) {

	}

	private static void aufgabe43() {
		System.out.println("A:");
		String[] exeA = {"2,1,-2", "1,2,-2", "2,1,-2", "3,1,1", "4,1,-3", "2,3,4", "4,3,5", "4,2,1", "3,2,-6", "2,3,-2", "3,2,3", "3,4,-14", "4," +
				"3,-3", "3,4,-3", "2,4,-56", "1,4,-17", "2,3,-17", "1,3,4", "1,2,53"};
		sMatA = execute(exeA, sMatA);

		System.out.println("B:");
		String[] exeB = {"2,1,10", "3,1,7", "4,1,7", "3,2,-2", "3,4,2", "4,3,2", "3,4,2", "4,3,4", "2,4,-20", "1,4,-2"};
		sMatB = execute(exeB, sMatB);

		System.out.println("C:");
		String[] exeC = {"1,4,-6", "2,4,-12", "3,4,3", "1,3,3", "2,3,5", "1,2,1", "2,1,-2", "3,1,5", "4,1,3", "3,2,4", "2,3,-5", "3,2,10", "1,2," +
				"-36", "4,2,-108", "4,3,13", "2,3,5", "1,3,3"};
		sMatC = execute(exeC, sMatC);
		//		System.out.println(printWA(sMatC));
		//		System.out.println("printWA(sMatC_): " + printWA(sMatC_));

		System.out.println("D:");
		String[] exeD = {"1,3,2", "2,3,4", "3,1,2", "3,4,-10", "2,4,-10", "4,2,4", "2,4,-3", "2,3,9", "3,2,6", "2,3,-2", "1,3,-6", "4,3,37", "1,2,"
				+ "2", "4,2,20", "1,4,41"};
		sMatD = execute(exeD, sMatD);
		System.out.println(printWA(sMatD));
	}

	private static void aufgabe44() {
		//		int[][] matAc = {{1, -1, 1, -3}, {0, 0, 0, -1}, {-1, 1, -1, 3}, {-1, 1, -1, 2}};
		//		int[][] matAn = {{-4, 6, -6, -10}, {0, 0, 0, -8}, {8, -8, 8, 8}, {10, -8, 10, -2}};
		//		String[] exe = {"3,1,1", "4,1,1", "4,2,-1", "1,3,1", "4,3,-1", "3,4,-2", "1,3,-2"};
		//		matAc = execute(exe, matAc);
		//		matAn = execute(exe, matAn);
		//		System.out.println(printWA(matAn));

		print44A(-8);
		print44A(8);

	}

	private static void print44A(int pC) {
		int[][] mat = {{pC - 4, -pC + 6, pC - 6, -3 * pC - 10}, {0, 0, 0, -pC - 8}, {-pC + 8, pC - 8, -pC + 8, 3 * pC + 8}, {-pC + 10, pC - 8, -pC +
				10, 2 * pC - 2}};
		System.out.println(pC + "\tdet(" + printWA(mat) + ')');
	}

	private static void aufgabe47() {
		//		// Eigenwert 1
		//		int[][] mat = {{1, -2, 0, -1, -1}, {2, 1, -2, 2, -2}, {0, 0, -2, 1, 0}, {0, 0, 1, 1, -2}, {0, 0, -2, 2, 1}};
		//		String[] exe = {"a,2,1,-2", "b,2,4,2", "b,3,4,2", "b,5,4,2", "c,3,2,-3", "c,5,2,-4", "d,5,3,-1", "d,4,2", "e,3,4", "e*,4,2", "f,3,4,
		// 4", "f,"
		//				+ "2,4,2", "f,1,4,1", "g,2,3,-1", "g,1,3,1"};
		//		mat = execute(exe, mat, 5);
		//		printMat(mat);

		// Eigenwert -2
		int[][] mat = {{-1, -2, 0, -1, -1}, {2, -1, -2, 2, -2}, {0, 0, 1, 1, 0}, {0, 0, 1, -1, -2}, {0, 0, -2, 2, -1}};
		String[] exe = {"a,2,1,2", "b,2,3,2", "b,4,3,-1", "b,5,3,2", "c,4,2,1", "c,5,2,-2", "d,5,4,2", "d,2,3", "e,3,4,-4", "e,1,4,-1", "f*,4,-1",
				"f*,3,3", "g,2,3,-1", "g,1,3,1", "g*,1,-1"};
		mat = execute(exe, mat, 5);
		printMat(mat);
	}

	private static void aufgabe47b() {
		for (int i = 0; i < 5; i++) {
			int a = (48 - 30 * (int) Math.pow(i, 2) + 23 * (int) Math.pow(i, 3) - 7 * (int) Math.pow(i, 4) + (int) Math.pow(i, 5)) % 5;
			int b = ((int) Math.pow(i, 2) - 5 * i + 18) % 5;
//			System.out.println("a: " + a + "\t" + i);
			System.out.println("b: " + b + "\t" + i);
		}
	}

	private static void aufgabe52(){
		int[][] mat = {{0,1,0,0},{0,1,0,1},{0,1,0,1},{0,0,0,0}};
	}

	public static int[][] execute(String[] pExe, int[][] pMat) {
		for (String exe : pExe) {
			String[] parts = exe.split(",");
			pMat = add(pMat, Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
		}
		System.out.println(printMat(pMat));
		return pMat;
	}

	public static int[][] execute(String[] pExe, int[][] pMat, int pPrime) {
		char cur = ' ';
		for (String exe : pExe) {
			String[] parts = exe.split(",");
			if (cur != parts[0].charAt(0)) {
				cur = parts[0].charAt(0);
				System.out.println(printMat2(pMat) + "\n");
			}

			if (parts.length == 3 && parts[0].contains("*")) {
				pMat = mult(pMat, Integer.parseInt(parts[1]) - 1, Integer.parseInt(parts[2]), pPrime);
				System.out.println("mul " + parts[1] + ", " + parts[2]);
			} else if (parts.length == 4) {
				pMat = add(pMat, Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]), pPrime);
				System.out.println("add " + parts[1] + ", " + parts[2] + ", " + parts[3]);
			} else if (parts.length == 3) {
				pMat = sw(pMat, Integer.parseInt(parts[1]) - 1, Integer.parseInt(parts[2]) - 1);
				System.out.println("sw " + parts[1] + ", " + parts[2]);
			}
		}
		System.out.println(printMat2(pMat) + "\n");
		return pMat;
	}

	public static String printMat2(int[][] pMat) {
		return Arrays.deepToString(pMat).replace("[[", "[").replace("]]", "]").replace("], ", "]\n").replace(", ", "\t");
	}

	public static String printMat(int[][] pMat) {
		return Arrays.deepToString(pMat).replace("[[", "[").replace("]]", "]").replace("], ", "]\n");
	}

	public static String printWA(int[][] pMat) {
		return Arrays.deepToString(pMat).replace("[", "{").replace("]", "}");
	}

	public static int[][] add(int[][] pMat, int pTo, int pFrom, int pMult) {
		int[][] res = new int[pMat.length][pMat[0].length];
		for (int i = 0; i < pMat.length; i++) {
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = pMat[i][j];
			}
		}
		for (int i = 0; i < pMat[pTo - 1].length; i++) {
			res[pTo - 1][i] += pMult * res[pFrom - 1][i];
		}
		return res;
	}

	public static int[][] add(int[][] pMat, int pTo, int pFrom, int pMult, int pPrime) {
		int[][] res = new int[pMat.length][pMat[0].length];
		for (int i = 0; i < pMat.length; i++) {
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = pMat[i][j];
			}
		}
		for (int i = 0; i < pMat[pTo - 1].length; i++) {
			res[pTo - 1][i] += pMult * res[pFrom - 1][i];
			res[pTo - 1][i] %= pPrime;
		}
		return res;
	}

	public static int[][] sw(int[][] pMat, int pTo, int pFrom) {
		int[][] res = new int[pMat.length][pMat[0].length];
		for (int i = 0; i < pMat.length; i++) {
			int x = i;
			if (x == pTo) {
				x = pFrom;
			} else if (x == pFrom) {
				x = pTo;
			}
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = pMat[x][j];
			}
		}
		return res;
	}

	public static int[][] mult(int[][] pMat, int pTo, int pTimes, int pPrime) {
		int[][] res = new int[pMat.length][pMat[0].length];
		int a = 1;
		for (int i = 0; i < pMat.length; i++) {
			if (i == pTo) {
				a = pTimes;
			} else {
				a = 1;
			}
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = a * pMat[i][j] % pPrime;
			}
		}
		return res;
	}
}
