package la.uebung_8;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 14.07.2017.
 */
public class AufgabeX {

	public static void main(String[] args) {
		//				int[][] mat = {{-1, -3, 3}, {2, 5, -4}, {2, 3, -2}};
		//		System.out.println(Arrays.deepToString(multiplyMatrices(multiplyMatrices(mat, mat, -1), mat, -1)));

		int[][] mat = {{-6, -1, 7}, {21, 4, -19}, {-3, -1, 4}};
		power(mat, 2015, 11);
		power(mat, 999, 11);
		power(mat, 331, 11);
	}

	private static void power(int[][] pMat, int pTimes, int pPrime) {
		int[][] cur = new int[pMat.length][pMat[0].length];
		for (int i = 0; i < cur.length; i++) {
			for (int j = 0; j < cur[0].length; j++) {
				cur[i][j] = pMat[i][j];
			}
		}

		for (int i = 1; i < pTimes; i++) {
			cur = multiplyMatrices(cur, pMat, pPrime);
		}

		System.out.println(Arrays.deepToString(cur));
	}

	/**
	 * Matrix Multiplikation
	 *
	 * @param m1 Matrix 1
	 * @param m2 Matrix 2
	 * @return Ergebnismatrix
	 */
	public static int[][] multiplyMatrices(int[][] m1, int[][] m2, int pPrime) {
		int[][] mat = null;

		if (m1[0].length == m2.length) {
			int zeilenm1 = m1.length;
			int spaltenm1 = m1[0].length;
			int spalenm2 = m2[0].length;

			mat = new int[zeilenm1][spalenm2];

			for (int i = 0; i < zeilenm1; i++) {
				for (int j = 0; j < spalenm2; j++) {
					mat[i][j] = 0;
					for (int k = 0; k < spaltenm1; k++) {
						mat[i][j] += m1[i][k] * m2[k][j];
					}
				}
			}
		} else {
			int zeilen = m1.length;
			int spalten = m1[0].length;

			mat = new int[zeilen][spalten];
			for (int i = 0; i < m1.length; i++) {
				for (int j = 0; j < m1[0].length; j++) {
					mat[i][j] = 0;
				}
			}
		}

		if (pPrime != -1) {
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat[i].length; j++) {
					mat[i][j] = mat[i][j] % pPrime;
				}
			}
		}

		return mat;
	}

}
