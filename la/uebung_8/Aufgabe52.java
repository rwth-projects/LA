package la.uebung_8;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 12.07.2017.
 */
public class Aufgabe52 {

	public static void main(String[] args) {

	}

	private static void mat4(){
		int[][] mat4 = {{6, -1, 2, 2}, {0, 7, -2, -1}, {0, 1, 4, 3}, {0, 0, 0, 0}};
		String[] exe = {};
		mat4 = execute(exe, mat4);
		printMat(mat4);
	}

	public static String printMat2(int[][] pMat) {
		return Arrays.deepToString(pMat).replace("[[", "[").replace("]]", "]").replace("], ", "]\n").replace(", ", "\t");
	}

	public static String printMat(int[][] pMat) {
		return Arrays.deepToString(pMat).replace("[[", "[").replace("]]", "]").replace("], ", "]\n");
	}

	public static int[][] execute(String[] pExe, int[][] pMat) {
		char cur = ' ';
		for (String exe : pExe) {
			String[] parts = exe.split(",");
			if (cur != parts[0].charAt(0)) {
				cur = parts[0].charAt(0);
				System.out.println(printMat2(pMat) + "\n");
			}

			if (parts.length == 3 && parts[0].contains("*")) {
				pMat = mult(pMat, Integer.parseInt(parts[1]) - 1, Integer.parseInt(parts[2]));
				System.out.println("mul " + parts[1] + ", " + parts[2]);
			} else if (parts.length == 4) {
				pMat = add(pMat, Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
				System.out.println("add " + parts[1] + ", " + parts[2] + ", " + parts[3]);
			} else if (parts.length == 3) {
				pMat = sw(pMat, Integer.parseInt(parts[1]) - 1, Integer.parseInt(parts[2]) - 1);
				System.out.println("sw " + parts[1] + ", " + parts[2]);
			}
		}
		System.out.println(printMat2(pMat) + "\n");
		return pMat;
	}

	public static int[][] add(int[][] pMat, int pTo, int pFrom, int pMult) {
		int[][] res = new int[pMat.length][pMat[0].length];
		for (int i = 0; i < pMat.length; i++) {
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = pMat[i][j];
			}
		}
		for (int i = 0; i < pMat[pTo - 1].length; i++) {
			res[pTo - 1][i] += pMult * res[pFrom - 1][i];
		}
		return res;
	}

	public static int[][] sw(int[][] pMat, int pTo, int pFrom) {
		int[][] res = new int[pMat.length][pMat[0].length];
		for (int i = 0; i < pMat.length; i++) {
			int x = i;
			if (x == pTo) {
				x = pFrom;
			} else if (x == pFrom) {
				x = pTo;
			}
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = pMat[x][j];
			}
		}
		return res;
	}

	public static int[][] mult(int[][] pMat, int pTo, int pTimes) {
		int[][] res = new int[pMat.length][pMat[0].length];
		int a = 1;
		for (int i = 0; i < pMat.length; i++) {
			if (i == pTo) {
				a = pTimes;
			} else {
				a = 1;
			}
			for (int j = 0; j < pMat[i].length; j++) {
				res[i][j] = a * pMat[i][j];
			}
		}
		return res;
	}
}
