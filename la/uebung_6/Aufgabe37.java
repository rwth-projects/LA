package la.uebung_6;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 26.06.2017.
 */
public class Aufgabe37 {

	public static void main(String[] args) {
		//(4.27)a) int[][] code = genCode(new int[][] {{1,1,0}, {0,1,1}});
		//(4.27)b) int[][] code = genCode(new int[][] {{1,0,1,0,1,0}, {0,1,0,1,0,1}});
		/*37)a)*/
		startCode("Aufgabe (37)a)", new int[][] {{0, 1, 1, 1, 1, 0, 0}, {1, 1, 0, 1, 0, 1, 0}, {1, 0, 1, 1, 0, 0, 1}}, 2);
		/*(37)b)*/
		startCode("Aufgabe (37)b)", new int[][] {{0, -2, -2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {-1, -1, -2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, {-1, -2, -1,
				0, 0, 1, 0, 0, 0, 0, 0, 0, 0}, {0, -2, -1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, {-2, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, {-2, -2, -2, 0,
				0, 0, 0, 0, 1, 0, 0, 0, 0}, {-2, -2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}, {-2, -1, -1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, {-1, -2, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 1, 0}, {-2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}}, 3);
		/*(37)c)*/
		startCode("Aufgabe (37)c)", new int[][] {{-3, -5, 1, 0, 0, 0, 0, 0}, {-2, -4, 0, 1, 0, 0, 0, 0}, {-1, -1, 0, 0, 1, 0, 0, 0}, {-1, -5, 0, 0,
				0, 1, 0, 0}, {-4, -5, 0, 0, 0, 0, 1, 0}, {-6, -1, 0, 0, 0, 0, 0, 1}}, 7);
		/*(37)d)*/
		startCode("Aufgabe (37)d)", new int[][] {{1, 1, 1, 0, 0}, {0, 0, 0, 1, 1}}, 2);

		//		System.out.println(hammingAbstand(new int[] {5, 3, 0, 1}, new int[] {-2, -4, 0, 1}));
	}

	private static void startCode(String pName, int[][] pSol, int pPrime) {
		new Thread(() -> {
			int[][] code = genCode(pSol, pPrime);
			System.out.println(pName + "minHammingAbstand: " + minHammingAbstand(code, pPrime, pName));
		}).start();
	}

	private static int[][] genCode(int[][] pSol, int pPrime) {
		int dif = 0;
		int[] quo = initZero(pSol.length);
		while (quo != null) {
			quo = iterQuoeffizieten(quo, pPrime);
			dif++;
		}
		//		System.out.println("dif: " + dif);

		int[][] res = new int[dif][pSol[0].length];
		dif = 0;
		quo = initZero(pSol.length);
		while (quo != null) {
			//			System.out.println("quo: " + Arrays.toString(quo));
			for (int i = 0; i < quo.length; i++) {
				for (int j = 0; j < pSol[i].length; j++) {
					res[dif][j] += quo[i] * pSol[i][j];
				}
			}
			//			System.out.println("res: " + Arrays.toString(res[dif]));

			quo = iterQuoeffizieten(quo, pPrime);
			dif++;
		}

		//		System.out.println(Arrays.deepToString(modulus(res, PRIME)));
		return res;
	}

	private static int[][] modulus(int[][] pArr, int pModulus) {
		for (int i = 0; i < pArr.length; i++) {
			for (int j = 0; j < pArr[i].length; j++) {
				pArr[i][j] = pArr[i][j] % pModulus;
			}
		}
		return pArr;
	}

	private static int[] initZero(int pWidth) {
		int[] arr = new int[pWidth];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = 0;
		}
		return arr;
	}

	private static int[] iterQuoeffizieten(int[] pQuo, int pPrime) {
		int cur = 0;
		while (true) {
			pQuo[cur]++;
			if (pQuo[cur] == pPrime) {
				pQuo[cur] = 0;
				cur++;
				if (cur == pQuo.length) {
					return null;
				}
			} else {
				return pQuo;
			}
		}
	}

	private static int minHammingAbstand(int[][] pArrs, int pPrime, String pAufg) {
		int minAbstand = Integer.MAX_VALUE;
		for (int i = 0; i < pArrs.length; i++) {
			for (int j = i + 1; j < pArrs.length; j++) {
				int curAbstand = hammingAbstand(pArrs[i], pArrs[j], pPrime);
				if (curAbstand < minAbstand) {
					minAbstand = curAbstand;
					System.out.println(pAufg + "\tminAbstand: " + minAbstand + '\t' + Arrays.toString(pArrs[i]) + '\t' + Arrays.toString(pArrs[j]));
				}
			}
		}
		return minAbstand;
	}

	private static int hammingAbstand(int[] pArr1, int[] pArr2, int pPrime) {
		if (pArr1.length != pArr2.length) {
			throw new IllegalArgumentException();
		}

		int abstand = 0;
		for (int i = 0; i < pArr1.length; i++) {
			if ((pArr1[i] - pArr2[i]) % pPrime != 0) {
				abstand++;
			}
		}
		return abstand;
	}
}
