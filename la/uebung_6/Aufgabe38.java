package la.uebung_6;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 30.06.2017.
 */
public class Aufgabe38 {

	private static final int[][] MAT_B = new int[][] {{1, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2}, {0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 2, 2, 2}, {0, 0, 0, 0,
			1, 1, 1, 1, 1, 1, 1, 1, 1}};
	private static final int PRIME = 3;

	public static void main(String[] args) {
		{
			int[] a = new int[] {2, 0, 2, 0, 0, 2, 1, 1, 1, 1, 2, 2, 0};
			System.out.println("a\t" + toWACloud(MAT_B, a));
			System.out.println(Arrays.toString(modulus(new int[] {13, 13, 10}, PRIME)));
			int[] anfA = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0};
			System.out.println("syn\t" + toWACloud(MAT_B, anfA));
			System.out.println("erg\t" + Arrays.toString(sub(a, anfA, PRIME)));
		}
		{
			int[] b = new int[] {0, 1, 2, 2, 0, 0, 0, 1, 0, 2, 0, 1, 2};
			System.out.println("\nb)");
			System.out.println("b\t" + toWACloud(MAT_B, b));
			System.out.println(Arrays.toString(modulus(new int[] {15, 14, 6}, PRIME)));
			int[] anfB = new int[] {0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			System.out.println("syn\t" + toWACloud(MAT_B, anfB));
			System.out.println("erg\t" + Arrays.toString(sub(b, anfB, PRIME)));
		}
		{
			int[] c = new int[] {2, 0, 2, 1, 2, 1, 2, 1, 2, 0, 2, 2, 0};
			System.out.println("\nc)");
			System.out.println("c\t" + toWACloud(MAT_B, c));
			System.out.println(Arrays.toString(modulus(new int[] {15, 14, 12}, PRIME)));
			int[] anfC = new int[] {0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			System.out.println("syn\t" + toWACloud(MAT_B, anfC));
			System.out.println("erg\t" + Arrays.toString(sub(c, anfC, PRIME)));
		}
		{
			int[] d = new int[] {0, 1, 0, 0, 2, 0, 1, 2, 1, 2, 2, 2, 0};
			System.out.println("\nd)");
			System.out.println("d\t" + toWACloud(MAT_B, d));

			System.out.println(Arrays.toString(modulus(new int[] {9, 14, 12}, PRIME)));
			int[] anfD = new int[] {0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			System.out.println("syn\t" + toWACloud(MAT_B, anfD));
			System.out.println("erg\t" + Arrays.toString(sub(d, anfD, PRIME)));
		}
		{
			int[] e = new int[] {1, 1, 2, 2, 0, 2, 1, 0, 0, 1, 0, 1, 1};
			System.out.println("\ne)");
			System.out.println("e\t" + toWACloud(MAT_B, e));

			System.out.println(Arrays.toString(modulus(new int[] {16, 10, 6}, PRIME)));
			int[] anfE = new int[] {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			System.out.println("syn\t" + toWACloud(MAT_B, anfE));
			System.out.println("erg\t" + Arrays.toString(sub(e, anfE, PRIME)));
		}
	}

	private static int[] modulus(int[] pArr, int pModulus) {
		for (int i = 0; i < pArr.length; i++) {
			pArr[i] = pArr[i] % pModulus;
		}
		return pArr;
	}

	private static String toWACloud(int[][] pMatA, int[] pMatB) {
		String out = "{";
		for (int i = 0; i < pMatA.length; i++) {
			out += "{";
			for (int j = 0; j < pMatA[i].length; j++) {
				out += pMatA[i][j];
				if (j < pMatA[i].length - 1) {
					out += ",";
				}
			}
			out += "}";
			if (i < pMatA.length - 1) {
				out += ",";
			}
		}
		out += "}.{";

		for (int i = 0; i < pMatB.length; i++) {
			out += "{" + pMatB[i] + "}";
			if (i < pMatB.length - 1) {
				out += ",";
			}
		}

		return out + "}";
	}

	private static int[] sub(int[] pMatA, int[] pMatB, int pPrime) {
		if (pMatA.length != pMatB.length) {
			throw new IllegalArgumentException();
		}
		int[] mat = new int[pMatA.length];
		for (int i = 0; i < pMatA.length; i++) {
			mat[i] = (pMatA[i] - pMatB[i] + pPrime) % pPrime;
		}
		return mat;
	}
}
