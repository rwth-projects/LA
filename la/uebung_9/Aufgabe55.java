package la.uebung_9;

import java.util.Arrays;

public class Aufgabe55 {

	public static void main(String[] args) {
		{
			int[][] matA1 = {{-1, 0, -1}, {-2, -2, 2}, {0, 0, -2}};
			int[][] matA2 = {{-1, -1, 1}, {-1, -1, -1}, {-1, 1, -3}};
			compare(matA1, matA2, 5);
			eig(matA1, matA2, 3, 4);
		}

		{
			int[][] matB1 = {{-3, 0, 1}, {5, 4, 1}, {-6, -6, -2}};
			int[][] matB2 = {{3, 0, -1}, {-8, 4, 4}, {18, -6, -8}};
			compare(matB1, matB2, Integer.MAX_VALUE);
			eig(matB1, matB2, 0, -2, 1);
		}

		{
			int[][] matC1 = {{-8, -4, 4}, {3, -1, -3}, {-1, -1, -3}};
			int[][] matC2 = {{-5, 2, 7}, {1, -5, -5}, {0, 1, -2}};
			compare(matC1, matC2, 13);
		}

		{

			int[][] matD1 = {{-1, 6, 12}, {2, -11, -23}, {-3, 6, 14}};
			int[][] matD2 = {{1, 8, 14}, {-2, -15, -27}, {-1, 8, 16}};
			compare(matD1, matD2, Integer.MAX_VALUE);
		}

		{
			int[][] matE1 = {{0, 1, 1}, {0, 1, 0}, {-2, 2, 3}};
			int[][] matE2 = {{3, 1, 1}, {-1, 1, -1}, {-1, -1, 1}};
			compare(matE1, matE2, 5);
		}
	}

	private static boolean compare(int[][] pMat1, int[][] pMat2, int pPrime) {
		System.out.println("gleicheSpur: " + gleicheSpur(pMat1, pMat2, pPrime));
		System.out.println("gleicheDet: " + gleicheDet(pMat1, pMat2, pPrime));
		chi(pMat1, pMat2);
		return true;
	}

	private static boolean gleicheSpur(int[][] pMat1, int[][] pMat2, int pPrime) {
		return spur(pMat1, pPrime) == spur(pMat2, pPrime);
	}

	private static int spur(int[][] pMat, int pPrime) {
		return pMat[0][0] + pMat[1][1] + pMat[2][2] % pPrime;
	}

	private static boolean gleicheDet(int[][] pMat1, int[][] pMat2, int pPrime) {
		return determinante(pMat1, pPrime) == determinante(pMat2, pPrime);
	}

	private static int determinante(int[][] pMat, int pPrime) {
		return pMat[0][0] * pMat[1][1] * pMat[2][2] + pMat[0][1] * pMat[1][2] * pMat[2][0] + pMat[0][2] * pMat[1][0] * pMat[2][1] - pMat[0][2] *
				pMat[1][1] * pMat[2][0] - pMat[0][0] * pMat[1][2] * pMat[2][1] - pMat[0][1] * pMat[1][0] * pMat[2][2] % pPrime;
	}

	private static void chi(int[][] pMat1, int[][] pMat2) {
		chi(pMat1);
		chi(pMat2);
	}

	private static void chi(int[][] pMat) {
		String str = "(x-" + pMat[0][0] + ")*(x-" + pMat[1][1] + ")*(x-" + pMat[2][2] + ")+(-" + pMat[0][1] + "*-" + pMat[1][2] + "*-" + pMat[2][0]
				+ ")+(-" + pMat[0][2] + "*-" + pMat[1][0] + "*-" + pMat[2][1] + ")-(-" + pMat[0][2] + "*(x-" + pMat[1][1] + ")*-" + pMat[2][0] + ")"
				+ "-" + "((x-" + pMat[0][0] + ")*-" + pMat[1][2] + "*-" + pMat[2][1] + ")-(-" + pMat[0][1] + "*-" + pMat[1][0] + "*(x-" + pMat[2][2]
				+ "))";
		System.out.println(str);
	}

	private static void eig(int[][] pMat1, int[][] pMat2, int... pEigenwerte) {
		System.out.println("mat1:");
		eig(pMat1, pEigenwerte);
		System.out.println("mat2:");
		eig(pMat2, pEigenwerte);
	}

	private static void eig(int[][] pMat, int... pEigenwerte) {
		for (int eig : pEigenwerte) {
			eig(pMat, eig);
		}
	}

	private static void eig(int[][] pMat, int pEigenwert) {
		pMat[0][0] -= pEigenwert;
		pMat[1][1] -= pEigenwert;
		pMat[2][2] -= pEigenwert;
		String str = "Eigenwert " + pEigenwert + "\t\tRowReduce[" + Arrays.deepToString(pMat).replace("[", "{").replace("]", "}") + "]";
		System.out.println(str);
	}
}
