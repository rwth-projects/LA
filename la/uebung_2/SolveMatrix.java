package la.uebung_2;

import java.util.Arrays;

public class SolveMatrix {

	public static void main(String[] args) {
		int[] arr = {0, 0, 0};
		int lu = 0;
		for (int i = 0; i < Math.pow(3, arr.length); i++) {
			arr = incr(arr, 3);
			System.out.println(Arrays.toString(arr));
//			double[][] dArr = convert(arr, 3);
//			if (istLinearUnabhaengig(dArr)) {
//
//				System.out.println("now");
//				for (int x = 0; x < arr.length; x++) {
//					for (int j = 0; j < arr.length; j++) {
//						System.out.print(" " + arr[x * arr.length + j]);
//					}
//					System.out.println();
//				}
//
//				solve(dArr, true);
//				solve(createMutation(convert(arr, 3)), true);
//				System.out.println();
//				lu++;
//				if (lu == 10000) {
//					return;
//				}
//			}
//			if (i % 1000 == 0) {
//				System.out.println("i: " + i);
//			}
		}
	}

	private static double[][] createMutation(double[][] pArr) {
		double[][] arr = new double[][] {{pArr[0][0] + pArr[0][2], pArr[1][0] + pArr[1][2], pArr[2][0] + pArr[2][2]}, {pArr[0][1] + pArr[0][2],
				pArr[1][1] + pArr[1][2], pArr[2][1] + pArr[2][2]}, {pArr[0][0] + pArr[0][1] - pArr[0][2], pArr[1][0] + pArr[1][1] - pArr[1][2],
				pArr[2][0] + pArr[2][1] - pArr[2][2]}};


		for (int i = 0; i < pArr.length; i++) {
			for (int j = 0; j < pArr.length; j++) {
				System.out.print(" " + pArr[i][j]);
			}
			System.out.println();
		}
		return arr;
	}

	private static int[] incr(int[] pArr, int pTill) {
		int pos = 0;

		while (true) {
			pArr[pos]++;
			if (pArr[pos] == pTill) {
				pArr[pos] = 0;
				pos++;
				if (pos == pArr.length) {
					throw new IllegalArgumentException();
				}
			} else {
				break;
			}
		}

		return pArr;
	}

	private static double[][] convert(int[] pArr, int pDim) {
		double[][] arr = new double[pDim][pDim];
		for (int x = 0; x < pDim; x++) {
			for (int y = 0; y < pDim; y++) {
				arr[x][y] = pArr[x * pDim + y];
			}
		}
		return arr;
	}

	private static boolean istLinearUnabhaengig(double[][] pMat) {
		double[][] res = solve(pMat, false);
		for (int i = 0; i < res.length; i++) {
			if (res[i][0] != 0) {
				return false;
			}
		}

		return true;
	}

	public static double[][] solve(double[][] pMat, boolean pPrint) {
		//		double[][] mat = {{3, -2, -3, 4}, {1, 1, -1, 1}, {-1, -1, 1, 3}, {-2, 2, 2, 1}};
		//		double[][] mat = {{1, 3, -1}, {1, -1, 3}, {2, 1, 3}};
		int n = pMat.length;
		double[][] constants = new double[n][1];
		for (int i = 0; i < n; i++) {
			constants[i][0] = 0;
		}

		if (pPrint) {
			char[] var = {'x', 'y', 'z', 'w'};
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {System.out.print(" " + pMat[i][j]);}
				System.out.print("  " + var[i]);
				System.out.print("  =  " + constants[i][0]);
				System.out.println();
			}
		}
		double inverted_mat[][] = invert(pMat);
		//		if (pPrint) {
		//			System.out.println("The inverse is: ");
		//			for (int i = 0; i < n; ++i) {
		//				for (int j = 0; j < n; ++j) {System.out.print(inverted_mat[i][j] + "  ");}
		//				System.out.println();
		//			}
		//		}
		double result[][] = new double[n][1];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 1; j++) {for (int k = 0; k < n; k++) {result[i][j] = result[i][j] + inverted_mat[i][k] * constants[k][j];}}
		}
		//		if (pPrint) {
		//			System.out.println("The product is:");
		//			for (int i = 0; i < n; i++) {
		//				System.out.println(result[i][0] + " ");
		//			}
		//		}

		return result;
	}

	public static double[][] invert(double a[][]) {
		int n = a.length;
		double x[][] = new double[n][n];
		double b[][] = new double[n][n];
		int index[] = new int[n];
		for (int i = 0; i < n; ++i) { b[i][i] = 1; }
		gaussian(a, index);
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {for (int k = 0; k < n; ++k) {b[index[j]][k] -= a[index[j]][i] * b[index[i]][k];}}
		}
		for (int i = 0; i < n; ++i) {
			x[n - 1][i] = b[index[n - 1]][i] / a[index[n - 1]][n - 1];
			for (int j = n - 2; j >= 0; --j) {
				x[j][i] = b[index[j]][i];
				for (int k = j + 1; k < n; ++k) {x[j][i] -= a[index[j]][k] * x[k][i];}
				x[j][i] /= a[index[j]][j];
			}
		}
		return x;
	}

	public static void gaussian(double a[][], int index[]) {
		int n = index.length;
		double c[] = new double[n];
		for (int i = 0; i < n; ++i) { index[i] = i; }
		for (int i = 0; i < n; ++i) {
			double c1 = 0;
			for (int j = 0; j < n; ++j) {
				double c0 = Math.abs(a[i][j]);
				if (c0 > c1) { c1 = c0; }
			}
			c[i] = c1;
		}
		int k = 0;
		for (int j = 0; j < n - 1; ++j) {
			double pi1 = 0;
			for (int i = j; i < n; ++i) {
				double pi0 = Math.abs(a[index[i]][j]);
				pi0 /= c[index[i]];
				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;
			for (int i = j + 1; i < n; ++i) {
				double pj = a[index[i]][j] / a[index[j]][j];
				a[index[i]][j] = pj;
				for (int l = j + 1; l < n; ++l) { a[index[i]][l] -= pj * a[index[j]][l]; }
			}
		}
	}
}