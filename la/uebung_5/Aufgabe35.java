package la.uebung_5;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 11.06.2017.
 */
public class Aufgabe35 {

	public static void main(String[] args) {
		int[][] t_ = new int[][] {{-1, -2, 0, 0}, {2, 3, 1, 2}, {-1, -2, -1, -2}, {0, 0, 1, 1}};
		int[][] u_ = new int[][] {add(t_[0]), add(t_[2]), add(t_[1], mult(-2, t_[2]), t_[3]), add(mult(3, t_[1]), mult(-6, t_[2]), mult(4, t_[3]))};
		int[][] v_ = new int[][] {add(t_[0]), add(t_[1], mult(2, t_[2])), add(mult(-4, t_[2]), mult(-3, t_[3])), add(mult(-1, t_[2]), mult(-1,
				t_[3]))};

		System.out.println("t: " + Arrays.deepToString(t_));
		System.out.println("u: " + Arrays.deepToString(u_));
		System.out.println("v: " + Arrays.deepToString(v_));

		System.out.println("\na)");
		System.out.println("phi(u_1): " + Arrays.toString(phi(u_[0])) + "\t\tWA: " + toWACloud(v_, phi(u_[0])));
		System.out.println("phi(u_2): " + Arrays.toString(phi(u_[1])) + "\t\tWA: " + toWACloud(v_, phi(u_[1])));
		System.out.println("phi(u_3): " + Arrays.toString(phi(u_[2])) + "\t\tWA: " + toWACloud(v_, phi(u_[2])));
		System.out.println("phi(u_4): " + Arrays.toString(phi(u_[3])) + "\t\tWA: " + toWACloud(v_, phi(u_[3])));

		System.out.println("\nb)");
		int[] addedU = add(u_[0], mult(-1, u_[1]), mult(-1, u_[2]), u_[3]);
		System.out.println("u1−u2−u3+u4: " + Arrays.toString(addedU));
		System.out.println("phi(addedU): " + Arrays.toString(phi(addedU)) + "\t\tWA: " + toWACloud(v_, phi(addedU)));

		System.out.println("\nc)");
		System.out.println("phi(t_2): " + Arrays.toString(phi(t_[1])) + "\t\tWA: " + toWACloud(t_, phi(t_[1])));

		System.out.println("\nd)");
		int[] addedT = add(t_[0], t_[1], t_[2], mult(-1, t_[3]));
		System.out.println("t1+t2+t3-t4: " + Arrays.toString(addedT));
		System.out.println("phi(addedT): " + Arrays.toString(phi(addedT)) + "\t\tWA: " + toWACloud(t_, phi(addedT)));
	}

	private static int[] phi(int[] pX) {
		int[] arr = new int[4];
		arr[0] = pX[0] * 7 + pX[1] * -3 + pX[2] - pX[3];
		arr[1] = pX[0] * 8 + pX[1] * -3 + pX[2] - pX[3];
		arr[2] = pX[0] * 2 + pX[1] * -1 + pX[2] * -7 + pX[3] * 4;
		arr[3] = pX[0] * 4 + pX[1] * -2 + pX[2] * -8 + pX[3] * 5;
		return arr;
	}

	public static int[] add(int[]... pArr) {
		int length = pArr[0].length;
		for (int[] ints : pArr) {
			if (ints.length != length) {
				throw new IllegalArgumentException("");
			}
		}
		int[] arr = new int[length];
		for (int i = 0; i < arr.length; i++) {
			for (int[] aPArr : pArr) {
				arr[i] += aPArr[i];
			}
		}
		return arr;
	}

	public static int[] mult(int pA, int[] pArr) {
		int[] arr = new int[pArr.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = pA * pArr[i];
		}
		return arr;
	}

	private static String toWACloud(int[][] pBase, int[] pArr) {
		return "rowreduce " + Arrays.deepToString(toBase(pBase, pArr)).replace("[", "{").replace("]", "}");
	}

	private static int[][] toBase(int[][] pBase, int[] pArr) {
		int[][] arr = new int[pBase.length][pBase[0].length + 1];
		for (int i = 0; i < pBase.length; i++) {
			for (int j = 0; j < pBase[i].length; j++) {
				arr[j][i] = pBase[i][j];
			}
		}
		for (int i = 0; i < pArr.length; i++) {
			arr[i][arr[i].length - 1] = pArr[i];
		}
		return arr;
	}
}
