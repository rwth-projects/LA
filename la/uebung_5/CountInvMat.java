package la.uebung_5;

public class CountInvMat {

	private static final int PRIM = 8;
	private static final int DIM = 3;
	private static int[] sMatrix = new int[DIM * DIM];

	public static void main(String[] args) {
		int matComb = 0;
		int inv = 0;
		while (true) {
			//			System.out.println(Arrays.toString(sMatrix));
			sMatrix = iterate(sMatrix);
			matComb++;
			if (sMatrix == null) {
				break;
			}
			if (determinante(sMatrix) % PRIM != 0) {
				inv++;
			}
			if (matComb % 10000 == 0) {
				System.out.println(matComb);
			}
		}
		System.out.println("Anzahl aller berechneten Matrizen: " + matComb + ", Anzahl aller möglichen Matrizen: " + (int) Math.pow(PRIM, DIM *
				DIM));
		System.out.println("Anzahl aller invertierbaren Matrizen: " + inv);
		System.out.println("Anzahl aller NICHT inverieren Matrizen: " + (matComb - inv));
	}

	private static int[] iterate(int[] pMat) {
		for (int i = 0; i < pMat.length; i++) {
			if (pMat[i] == PRIM - 1) {
				pMat[i] = 0;
			} else {
				pMat[i]++;
				return pMat;
			}
		}
		return null;
	}

	private static int determinante(int[] pMat) {
		return pMat[0] * pMat[4] * pMat[8] + pMat[1] * pMat[5] * pMat[6] + pMat[2] * pMat[3] * pMat[7] - pMat[6] * pMat[4] * pMat[2] - pMat[7] *
				pMat[5] * pMat[0] - pMat[8] * pMat[3] * pMat[1];
	}
}
