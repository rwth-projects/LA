package la.uebung_5;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 10.06.2017.
 */
public class Aufgabe26 {

	public static void main(String[] args) {
		int[][] s = new int[][] {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
		int[][] t = new int[][] {{1, 2, 2, 0}, {-1, -1, -1, 0}, {-1, 0, 1, 2}, {1, 0, -1, -1}};
		int[][] u = new int[][] {add(t[0]), add(t[2]), add(t[1], mult(-2, t[2]), t[3]), add(mult(3, t[1]), mult(-6, t[2]), mult(4, t[3]))};
		int[][] v = new int[][] {add(t[0]), add(t[1], mult(2, t[2])), add(mult(-4, t[2]), mult(-3, t[3])), add(mult(-1, t[2]), mult(-1, t[3]))};
		System.out.println("s: " + Arrays.deepToString(s));
		System.out.println("t: " + Arrays.deepToString(t));
		System.out.println("u: " + Arrays.deepToString(u));
		System.out.println("v: " + Arrays.deepToString(v));

		//		System.out.println("\na)");
		//		korsp(s, t[0]);
		//		korsp(s, t[1]);
		//		korsp(s, t[2]);
		//		korsp(s, t[3]);
		//
		//		System.out.println("\nb)");
		//		int[] addedS = {-1, 1, -1, 1};
		//		korsp(s, phi(addedS));
		//
		//		System.out.println("\nc)");
		//		int[] addedU = add(u[0], u[1], mult(-1, u[2]), u[3]);
		//		korsp(v, phi(addedU));
		//
		//		System.out.println("\nd)");
		//		korsp(s, u[0]);
		//		korsp(s, u[1]);
		//		korsp(s, u[2]);
		//		korsp(s, u[3]);
		//
		//		System.out.println("\ne)");
		//		korsp(s, phi(s[0]));
		//		korsp(s, phi(s[1]));
		//		korsp(s, phi(s[2]));
		//		korsp(s, phi(s[3]));
		//
		//		System.out.println("\nf)");
		//		korsp(v, phi(u[3]));
		//
		//		System.out.println("\ng)");
		//		korsp(t, s[0]);
		//		korsp(t, s[1]);
		//		korsp(t, s[2]);
		//		korsp(t, s[3]);
		//
		//		System.out.println("\nh)");
		//		int[] addedT = add(t[0], t[1], mult(-1, t[2]), t[3]);
		//		korsp(t, phi(addedT));
		//
		//		System.out.println("\ti)");
		//		korsp(t, phi(t[0]));
		//		korsp(t, phi(t[1]));
		//		korsp(t, phi(t[2]));
		//		korsp(t, phi(t[3]));


		System.out.println("Marcos 26");
		System.out.println("\na)");
		korsp(t, t[0]);
		korsp(t, t[1]);
		korsp(t, t[2]);
		korsp(t, t[3]);

		System.out.println("\nb)");
		int[] addedS = add(mult(-1, s[0]), s[1], mult(-1, s[2]), s[3]);
		korsp(s, addedS);

		System.out.println("\nf)");
		korsp(v, phi(u[3]));

		System.out.println("Daniel:");
		korsp(v, phi(u[3]));
	}

	private static void korsp(int[][] pBase, int[] pInput) {
		System.out.println(Arrays.toString(pInput) + "\t\tWA: " + toWACloud(pBase, pInput));
	}

	private static int[] phi(int[] pX) {
		int[] arr = new int[4];
		arr[0] = pX[0] * 7 + pX[1] * -3 + pX[2] - pX[3];
		arr[1] = pX[0] * 8 + pX[1] * -3 + pX[2] - pX[3];
		arr[2] = pX[0] * 2 + pX[1] * -1 + pX[2] * -7 + pX[3] * 4;
		arr[3] = pX[0] * 4 + pX[1] * -2 + pX[2] * -8 + pX[3] * 5;
		return arr;
	}

	private static int[] add(int[]... pArr) {
		int length = pArr[0].length;
		for (int[] ints : pArr) {
			if (ints.length != length) {
				throw new IllegalArgumentException("");
			}
		}
		int[] arr = new int[length];
		for (int i = 0; i < arr.length; i++) {
			for (int[] aPArr : pArr) {
				arr[i] += aPArr[i];
			}
		}
		return arr;
	}

	private static int[] mult(int pA, int[] pArr) {
		int[] arr = new int[pArr.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = pA * pArr[i];
		}
		return arr;
	}

	private static String toWACloud(int[][] pBase, int[] pArr) {
		return "rowreduce " + Arrays.deepToString(toBase(pBase, pArr)).replace("[", "{").replace("]", "}");
	}

	private static int[][] toBase(int[][] pBase, int[] pArr) {
		int[][] arr = new int[pBase.length][pBase[0].length + 1];
		for (int i = 0; i < pBase.length; i++) {
			for (int j = 0; j < pBase[i].length; j++) {
				arr[j][i] = pBase[i][j];
			}
		}
		for (int i = 0; i < pArr.length; i++) {
			arr[i][arr[i].length - 1] = pArr[i];
		}
		return arr;
	}
}
