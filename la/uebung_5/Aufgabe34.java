package la.uebung_5;

import java.util.Arrays;

/**
 * Created by Marc Schmidt on 09.06.2017.
 */
public class Aufgabe34 {

	public static void main(String[] args) {
		int[][] s = new int[][] {{-4, 2, 2, 0}, {5, 0, 0, 1}, {1, 2, 2, 2}};
		int[][] t = new int[][] {{5, 4, 4, 4}, {0, -5, -5, -3}, {3, 3, 3, -3}};
		int[][] u = new int[][] {{-1, 4, 4, -2}, {-5, -4, -4, 2}, {3, 0, 0, -5}};
		int[][] v = new int[][] {{0, 3, 3, -5}, {3, 0, 0, -1}, {0, -4, -4, -2}};

		//Marc
//		int[][] base = t;
//		for (int i = 0; i < base.length; i++) {
//			int[] ints = base[i];
//
//			// (σ◦ρ◦ψ)
//			int[] comp = compSigma(compRho(compPsi(ints)));
//			int[] mod = mod(comp, 11);
//			int[] trans = trans(mod);
//			System.out.println("base_" + i + "\ta(" + Arrays.toString(ints) + ")\t" + Arrays.toString(comp) + "\tmodulo: " + Arrays.toString(mod) +
//					"\tStandardtransversale: " + Arrays.toString(trans) + "\tWA Cloud:\t\t" + toWACloud(t, trans));
//		}

		//Daniel:
		int[][] base = s;
		for (int i = 0; i < base.length; i++) {
			int[] ints = base[i];

			// (σ◦ρ◦τ)
			int[] comp = compSigma(compRho(compTau(ints)));
			int[] mod = mod(comp, 11);
			int[] trans = trans(mod);
			System.out.println("base_" + i + "\ta(" + Arrays.toString(ints) + ")\t" + Arrays.toString(comp) + "\tmodulo: " + Arrays.toString(mod) +
					"\tStandardtransversale: " + Arrays.toString(trans) + "\tWA Cloud:\t\t" + toWACloud(s, trans));
		}
	}

	/*ϕ*/
	private static int[] compPhi(int[] pArr) {
		return new int[] {-1 * pArr[0] - 5 * pArr[1] + 5 * pArr[3], 4 * pArr[0] - 3 * pArr[1] + 3 * pArr[3], 4 * pArr[0] - 3 * pArr[1] + 3 *
				pArr[3], -2 * pArr[0] - 4 * pArr[1] + 5 * pArr[3]};
	}

	/*ψ*/
	private static int[] compPsi(int[] pArr) {
		return new int[] {1 * pArr[0] + 3 * pArr[1] - 5 * pArr[3], -2 * pArr[0] + 5 * pArr[1] - 1 * pArr[3], -2 * pArr[0] + 5 * pArr[1] - 1 *
				pArr[3], -3 * pArr[0] + 2 * pArr[1] + 4 * pArr[3]};
	}

	/*ρ*/
	private static int[] compRho(int[] pArr) {
		return new int[] {4 * pArr[0] - 5 * pArr[1] - 3 * pArr[3], -3 * pArr[0] + 4 * pArr[1] - 3 * pArr[3], -3 * pArr[0] + 4 * pArr[1] - 3 *
				pArr[3], -5 * pArr[0] + 3 * pArr[1] - 5 * pArr[3]};
	}

	/*σ*/
	private static int[] compSigma(int[] pArr) {
		return new int[] {-3 * pArr[0] - 1 * pArr[1] - 5 * pArr[3], -4 * pArr[0] - 2 * pArr[1] - 4 * pArr[3], -4 * pArr[0] - 2 * pArr[1] - 4 *
				pArr[3], -2 * pArr[0] - 2 * pArr[1] - 3 * pArr[3]};
	}

	/*τ*/
	private static int[] compTau(int[] pArr) {
		return new int[] {-1 * pArr[0] - 4 * pArr[1] + 2 * pArr[3], -4 * pArr[0] + 5 * pArr[1] - 2 * pArr[3], -4 * pArr[0] + 5 * pArr[1] - 2 *
				pArr[3], -3 * pArr[0] + 5 * pArr[3]};
	}

	private static int[] mod(int[] pArr, int pModulo) {
		int[] arr = new int[pArr.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = pArr[i] % pModulo;
		}
		return arr;
	}

	private static int[] trans(int[] pArr) {
		int[] arr = new int[pArr.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = pArr[i];
			if (arr[i] > 5) {
				arr[i] -= 11;
			}
		}
		return arr;
	}

	private static String toWACloud(int[][] pBase, int[] pArr) {
		return "RowReduce[" + Arrays.deepToString(toBase(pBase, pArr)).replace("[", "{").replace("]", "}") + ",{Modulus->11}]";
	}

	private static int[][] toBase(int[][] pBase, int[] pArr) {
		int[][] arr = new int[pBase.length + 1][pArr.length];
		for (int i = 0; i < pBase.length; i++) {
			for (int j = 0; j < pBase[i].length; j++) {
				arr[j][i] = pBase[i][j];
			}
		}
		for (int i = 0; i < pArr.length; i++) {
			arr[i][arr[i].length - 1] = pArr[i];
		}
		return arr;
	}
}
