package la.other;

/**
 * Created by Marc Schmidt on 20.07.2017.
 */
public class FiniteFieldPower {

	public static void main(String[] args) {
		for (int x = 0; x < 5; x++) {
			//			System.out.println(x + "\t" + ((x * x * x + 4 * x * x + 6 * x + 3) % 7));
			//			System.out.println(x + "\t" + (((x + 3) * (x + 1) * (x + 1) + 5 * 2 * 4 - 8 * (1 + x) - 6 * (x + 3)) % 7) + "\t" + ((x * x *
			// x - 16 * x
			//					* x + 49 * x - 4) % 7));
			//			System.out.println(x + "\t" + (((x + 6) * (x + 6) * (x + 4) + 2 * 5 * 4 - 6 * (x + 6) - 16 * (x + 6))%7));
			//			System.out.println(x + "\t" + (((x + 5) * (x + 3) * (x + 2) + 50 - 25 * (x + 5) - 30 * (x + 2)) % 7));
			//			System.out.println(x + "\t" + ((x * x * x + 3 * x * x + 4 * x + 4) % 7) + "\t" + ((x * x * (x + 3) + 30 - 18 * (x + 3) - 6 *
			// x) % 7) +
			//					"\t" + (((x - 3) * (x * x + 6 * x + 1)) % 7) + "\t" + (((x - 3) * (x - 3) * (x + 9)) % 7));
			System.out.println(x + "\t" + (((x + 1) * (x + 2) * (x + 2)) % 5) + "\t" + (((x + 1) * (x + 1) * (x + 3) + 1 + 16 - 4 * (x + 1) - 4 * (x
					+ 1) - (x + 3)) % 5));
		}
	}
}
