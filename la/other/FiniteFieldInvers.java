package la.other;

/**
 * Created by Marc Schmidt on 17.06.2017.
 */
public class FiniteFieldInvers {

	private static final int[] FINITE_FIELDS = {2, 3, 5, 7, 11, 13, 17, 19, 23};

	public static void main(String[] args) {
		for (int finiteField : FINITE_FIELDS) {
			System.out.println(finiteFieldInvers(finiteField));
		}
	}

	private static String finiteFieldInvers(int pFiniteField) {
		String invers = "field: " + pFiniteField + "\t{";
		for (int i = 0; i < pFiniteField; i++) {
			if (hasInvers(pFiniteField, i)) {
				invers += i + ", ";
			}
		}
		return (invers + '}').replace(", }", "}");
	}

	private static boolean hasInvers(int pFiniteField, int pNum) {
		for (int i = 0; i < pFiniteField; i++) {
			if (isInvers(pFiniteField, pNum, i)) {
//				System.out.println("field: " + pFiniteField + ", num: " + pNum + ", invers: " + i);
				return true;
			}
		}
		return false;
	}

	private static boolean isInvers(int pFiniteField, int pNum, int pInvers) {
		return pNum * pInvers % pFiniteField == 1;
	}
}
